from django.contrib import admin
from receipts.models import Receipt, Account, Category

# Register your models here.

class ReceiptAdmin(admin.ModelAdmin):
    pass


class AccountAdmin(admin.ModelAdmin):
    pass


class CategoryAdmin(admin.ModelAdmin):
    pass


admin.site.register(Receipt, ReceiptAdmin)
admin.site.register(Account, AccountAdmin)
admin.site.register(Category, CategoryAdmin)