from django.urls import path, include, reverse_lazy
from django.views.generic.base import RedirectView
from receipts.views import (
    ReceiptListView,
    ReceiptCreateView,
    AccountListView,
    AccountCreateView,
    CategoryListView,
    CategoryCreateView,
)


urlpatterns = [
    path('', ReceiptListView.as_view(), name='home'),
    path('create/', ReceiptCreateView.as_view(), name='receipt_create'),
    path('accounts/', AccountListView.as_view(), name='account_list'),
    path('accounts/create/', AccountCreateView.as_view(), name='account_create'),
    path('categories/', CategoryListView.as_view(), name='category_list'),
    path('categories/create/', CategoryCreateView.as_view(), name='category_create')
]
