from django.db import models
from django.conf import settings

# Create your models here.

USER_MODEL = settings.AUTH_USER_MODEL


class Receipt(models.Model):
    vendor = models.CharField(max_length=100)
    total = models.DecimalField(max_digits=10, decimal_places=3)
    tax = models.DecimalField(max_digits=10, decimal_places=3)
    date = models.DateField(null=True, blank=False)
    purchaser = models.ForeignKey(
        USER_MODEL,
        related_name='receipts',
        on_delete=models.CASCADE,
        null=True
    )
    category = models.ForeignKey(
        "Category",
        related_name="receipts",
        on_delete=models.CASCADE
    )
    account = models.ForeignKey(
        "Account",
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return str(self.purchaser) + "'s " + "receipt"


class Category(models.Model):
    name = models.CharField(max_length=100)
    owner = models.ForeignKey(
        USER_MODEL,
        related_name='categories',
        on_delete=models.CASCADE,
        null=True
    )
    def __str__(self):
        return self.name


class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        USER_MODEL,
        related_name='accounts',
        on_delete=models.CASCADE,
        null=True
    )
    def __str__(self):
        return self.name 

