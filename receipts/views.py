from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from receipts.models import Receipt, Account, Category
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = 'receipts/list.html'

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(CreateView):
    model = Receipt
    template_name = 'receipts/create.html'
    fields = ['vendor', 'total', 'tax', 'date', 'category', 'account']
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        return redirect("home")


class AccountListView(ListView):
    model = Account
    template_name = 'receipts/accounts/list.html'
    
    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class AccountCreateView(CreateView):
    model = Account
    template_name = 'receipts/accounts/create.html'
    fields = ['name', 'number']
    success_url = reverse_lazy('account_list')

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("account_list")   


class CategoryListView(ListView):
    model = Category
    template_name = 'receipts/categories/list.html'

    def get_queryset(self):
        return Category.objects.filter(owner=self.request.user)


class CategoryCreateView(CreateView):
    model = Category
    template_name = 'receipts/categories/create.html'
    fields = ['name']
    success_url = reverse_lazy('category_list')

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("category_list")   

    