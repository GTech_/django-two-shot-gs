from django.urls import path, include, reverse_lazy
from django.views.generic.base import RedirectView
from django.contrib.auth.views import LoginView, LogoutView
from accounts.views import signup


urlpatterns = [
    path('signup/', signup, name='signup'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),

]
